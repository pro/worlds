# worlds

hangout with your friends and communities online. chat and voice communication. we aim to put users first by building a beautiful application that delivers a great user experience, while also caring and respecting user privacy.

made with ❤️ in 🇬🇧